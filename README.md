## IkiWiki [Plugins][1]

These are plugins I've written for [IkiWiki](http://ikiwiki.info),
which is a WikiWIki that keeps it's source files in an RCS system like Git or
Subversion.

Most of the plugins contained here are simply tweaked a little from the
original sources for my own use and probably not of interest to anyone else.  A
few are original and are probably of interest though.  These include:

* [LifeFyre for Ikiwiki](https://bitbucket.org/PointDefiance/ikiwiki/src/master/Plugin/livefyre.pm)

For documentation on installation, please read the sourcefile for Perl POD
Documentation.  I've also posted an article on how to use the livefyre plugin
at <http://point-defiance.rhcloud.com/Articles/IkiWikiAndLiveFyre/>.

## IkiWiki [Pingbacks][3]

I've adapted [Ian Hickson's original perl reference pingback implementation][2]
to a sort of IkiWiki implementation.  It works in two parts, a server and
client impementation.  

The server is a perl script which waits for remote sites to ping your wiki, and
on receiving a valid ping, visits the requesting website and scrapes some title
information.  Assuming nothing fails or looks odd, it creates a new (or
updates) a unqiue markdown file in your IkiWiki underlay cooresponding to the
articles pingback requests.  

The client is another perl script which is used to scan articles you have
written in ikiwiki for links to external sites.  It visits each of those sites,
looking for a pingback server corresponding to each link.  If one is found, it
tries to generate a pingback request.

The full documentation can be found in the pingback subdirectory.

[1]: https://bitbucket.org/PointDefiance/ikiwiki/src/master/Plugin?at=master
    "Plugins"
[2]: http://software.hixie.ch/utilities/cgi/pingback-proxy/
    "Ian Hickson's Pingback Proxy"
[3]: https://bitbucket.org/PointDefiance/ikiwiki/src/master/pingback
    "IkiWiki Pingback Implementation"
