#!/usr/bin/perl

package IkiWiki::Plugin::randomtitle;

use warnings;
use strict;
use IkiWiki 3.00;

our $VERSION = '0.05';

sub import {
    hook( type => 'pagetemplate', id   => 'randomtitle', call => \&randomtitle );
    hook( type => "checkconfig",  id   => 'randomtitle', call => \&checkconfig );
}

sub checkconfig () {
    if (! defined $config{randomtitlefile} ) {
        $config{randomtitlefile} = '/home/defiance/.ikiwiki/randomtitles.txt';
    }
}

sub randomtitle (@) {
    my %params=@_;
    my $template=$params{template};

    my $rand_line;
    my @titlearray;

    # So we don't re-read the array over and over
    if ( ! @titlearray ) {
        open RANDFILE, "< $config{randomtitlefile}" or die "Could not open $config{randomtitlefile}: $!\n";
        @titlearray=<RANDFILE>;
        close RANDFILE;
    }

    $rand_line = $titlearray[rand @titlearray];

    if (defined $rand_line && length $rand_line) {
        $template->param(randomtitle => $rand_line);
    }

}

1;

__END__

