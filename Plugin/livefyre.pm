package IkiWiki::Plugin::livefyre;
use warnings;
use strict;
use JSON;
use JSON::WebToken;
use Digest::MD5 qw(md5_hex);

use IkiWiki 3.00;

our $VERSION = '0.05';

sub import {

    # template hook
    hook(
        type => 'pagetemplate',
        id   => 'livefyre',
        call => \&livefyre_comments
    );
}

# Template params:
#  LIVEFYRE_COMMENTS  : full livefyre comments
#  LIVEFYRE_COUNT     : count link
sub livefyre_comments {
    my %args = @_;

    $args{template}->param( 'LIVEFYRE' => 1 );

    # Only place the livefyre code on the pages not excluded.  This means
    # that the livefyre code will not appear when a post is embedded in
    # the index, recentchanges, tag pages.

    if ( $args{page} eq $args{destpage} ) {

        # Declare some varibles to keep the code cleaner...
        my $perl_siteid  = $config{'livefyre_siteid'};
        my $perl_sitekey = $config{'livefyre_sitekey'};
        my $perl_ident   = $args{destpage};
        my $perl_title   = pagetitle( $args{destpage} );
        my $perl_url     = urlto( $args{destpage}, 'index', '1' );
           $perl_url     =~ s{(?<!http:)//}{/}g;
           # Above regexp removes extra slashes produced by urlto, odd

        # Skip processing on excluded pages
        foreach my $pattern ( @{ $config{livefyre_skip_patterns} } ) {
            if ( $perl_ident =~ $pattern ) {
                return '';
            }
        }

        # Create CollectionMeta ArrayOfHashes,
        # https://github.com/Livefyre/livefyre-docs/wiki/StreamHub-Integration-Guide
        # created line by line mostly because code looks nicer
        #
        #
        my @CollectionMeta;
        $CollectionMeta[0]{title} = $perl_title;
        $CollectionMeta[0]{url}   = $perl_url;
        $CollectionMeta[0]{tags}  = "";

        # Create a checksum based on above attributes
        my $checksum = md5_hex( encode_json \@CollectionMeta );

        # Now add calculated Checksum and articleId to Collection
        $CollectionMeta[0]{articleId} = $perl_ident;
        $CollectionMeta[0]{checksum}  = $checksum;

        # Encode it all into a JWT Token using sitekey
        my $webtoken = JSON::WebToken->encode( @CollectionMeta, $perl_sitekey );
        
        # Tweak CSS a bit, mostly to increase size of ridiculous small font in
        # the editor. 
        # https://github.com/Livefyre/livefyre-docs/wiki/Comment-Editor-Customization
        #
        my $editor_css = 
            {
                "background"     => '#fff',
                "color"          => '#000',
                "font-family"    => '"Museo Sans", Helvetica, Arial',
                "font-size"      => '0.9em'
            };

        # Tweak text strings a bit (not working yet)
        # https://github.com/Livefyre/livefyre-docs/wiki/Customizing-Text-Strings
        #
        my $custom_text =
            {
                "signInGuest"    => 'Sign in or Post Anonymously',
                "signIn"         => 'Sign in via Livefyre'
            };

        # Disable Linked-In Integration (Only Facebook + Twitter)
        # https://github.com/Livefyre/livefyre-docs/wiki/%22Post-to%22-Button-Customization
        #
        my $post_providers = [ 'tw', 'fb' ];

        # Create Comment Stream Config Object (ArrayOfHashes)
        my @fyre_config = (
            {
                "el"             => 'livefyre-comments',
                "checksum"       => $checksum,
                "collectionMeta" => $webtoken,
                "siteId"         => $perl_siteid,
                "articleId"      => $perl_ident,
                "editorCss"      => $editor_css,
                "postToButtons"  => $post_providers,
                "strings"        => $custom_text
            }
        );

        my $commentStreamConfig = encode_json( \@fyre_config );

        # Javascript Output
        my $jscript = <<LIVEFYRECOMMENTS;
<script type="text/javascript" src="http://zor.livefyre.com/wjs/v3.0/javascripts/livefyre.js"></script>
<script type="text/javascript">var lf_config = $commentStreamConfig;var conv = fyre.conv.load({}, lf_config);</script>
<noscript>Please enable JavaScript to view the comments powered by LiveFyre.</noscript>
LIVEFYRECOMMENTS

        $args{template}->param( 'LIVEFYRE_COMMENTS' => $jscript );
    }
    else {
        # this is not the page but an embedded page.
    }
}

1;

__END__

=head1 NAME

IkiWiki::Plugin::livefyre - Add LiveFyre comments to IkiWiki pages

=head1 DESCRIPTION

This plugin makes it easy for you to add LiveFyre comments to pages
and blog posts in IkiWiki. It also provides a way to allow you
to prevent comments from being posted on certain pages.

=head1 INSTALLATION

Put F<livefyre.pm> in F<$HOME/.ikiwiki/IkiWiki/Plugin/> or elsewhere in
your C<@INC> path.

=head1 CONFIGURATION

Add to the configuration in your F<ikiwiki.setup> file.

    ## LiveFyre plugin
    # Your livefyre "siteid" and "sitekey"
    #
    # livefyre_siteid: '<siteidnumber>'
    # livefyre_sitekey: '<sitekeytext>'
    #
    # A list of regular expressions matching pages that should
    # not have the livefyre comments placed on them.
    #
    # Example:
    # livefyre_skip_patterns:
    # - ^index$
    # - ^posts$
    # - ^ikiwiki$
    # - ^templates$
    # - ^smileys$
    # - ^recentchanges$
    # - ^tags/

Add C<livefyre> to the list of plugins:

    add_plugins:
    - goodstuff
    - livefyre

You should also turn off the comments plugin

    disable_plugins:
    - comments

=head1 TEMPLATES

You will need to add the following code to F<page.tmpl>. I suggest putting
it just before the C<COMMENTS> block or after the C<content> div.

<TMPL_UNLESS DYNAMIC>
 <TMPL_IF LIVEFYRE>
 <div id="livefyre-comments"></div>
 </TMPL_IF>
</TMPL_UNLESS>

DYNAMIC prevents livefyre from loading during "live" operations like editing
wikitext or searching.  You will also need to add the following at the end of
your template right before you close the body.

<TMPL_UNLESS DYNAMIC>
 <TMPL_IF LIVEFYRE>
  <TMPL_VAR LIVEFYRE_COMMENTS> <!-- load javascript at end to prevent blocking -->
 </TMPL_IF>
</TMPL_UNLESS>

This ensures the javascript doesn't block.  Livefyre isn't supposed to block
at all but I noticed that page rendering sped up by stashing the javascript
calls at the end after content had been loading.

=head1 BUGS AND LIMITATIONS

Report bugs at https://bitbucket.org/PointDefiance/ikiwiki/issues?status=new&status=open

=head1 TODO

* Tagging (not sure how at the moment)
* Single Signon (seperate plugin?)

=head1 AUTHOR(S)

Charles Mauch <cmauch@gmail.com>, plugin originally based on the disqus plugin
develeped by Randall Smith <perlstalker@vuser.org>

=head1 LICENSE AND COPYRIGHT

Copyright (C) 2013 Charles Mauch

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

=head1 SEE ALSO

=over 4

=item http://ikiwiki.info/

=item https://github.com/Livefyre/livefyre-docs/wiki

=back
