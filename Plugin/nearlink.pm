#!/usr/bin/perl
# Sidebar plugin.
# by Tuomo Valkonen <tuomov at iki dot fi>
# Tweaked for nearlinks, no big changes other than name
# Charles Mauch <cmauch at gmail.com>

package IkiWiki::Plugin::nearlink;

use warnings;
use strict;
use IkiWiki 3.00;

sub import {
	hook(type => "getsetup", id => "nearlink", call => \&getsetup);
	hook(type => "preprocess", id => "nearlink", call => \&preprocess);
	hook(type => "pagetemplate", id => "nearlink", call => \&pagetemplate);
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => 1,
		},
}

my %pagenearlink;

sub preprocess (@) {
	my %params=@_;

	my $page=$params{page};
	return "" unless $page eq $params{destpage};
	
	if (! defined $params{content}) {
		$pagenearlink{$page}=undef;
	}
	else {
		my $file = $pagesources{$page};
		my $type = pagetype($file);

		$pagenearlink{$page}=
			IkiWiki::htmlize($page, $page, $type,
			IkiWiki::linkify($page, $page,
			IkiWiki::preprocess($page, $page, $params{content})));
	}

	return "";
}

my $oldfile;
my $oldcontent;

sub nearlink_content ($) {
	my $page=shift;
	
	return delete $pagenearlink{$page} if defined $pagenearlink{$page};

	my $nearlink_page=bestlink($page, "nearlink") || return;
	my $nearlink_file=$pagesources{$nearlink_page} || return;
	my $nearlink_type=pagetype($nearlink_file);
	
	if (defined $nearlink_type) {
		# FIXME: This isn't quite right; it won't take into account
		# adding a new nearlink page. So adding such a page
		# currently requires a wiki rebuild.
		add_depends($page, $nearlink_page);

		my $content;
		if (defined $oldfile && $nearlink_file eq $oldfile) {
			$content=$oldcontent;
		}
		else {
			$content=readfile(srcfile($nearlink_file));
			$oldcontent=$content;
			$oldfile=$nearlink_file;
		}

		return unless length $content;
		return IkiWiki::htmlize($nearlink_page, $page, $nearlink_type,
		       IkiWiki::linkify($nearlink_page, $page,
		       IkiWiki::preprocess($nearlink_page, $page,
		       IkiWiki::filter($nearlink_page, $page, $content))));
	}

}

sub pagetemplate (@) {
	my %params=@_;

	my $template=$params{template};
	if ($params{destpage} eq $params{page} &&
	    $template->query(name => "nearlink")) {
		my $content=nearlink_content($params{destpage});
		if (defined $content && length $content) {
		        $template->param(nearlink => $content);
		}
	}
}

1
