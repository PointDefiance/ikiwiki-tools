#!/usr/bin/perl
# Sidebar plugin.
# by Tuomo Valkonen <tuomov at iki dot fi>
# Tweaked for refmetas, no big changes other than name
# Charles Mauch <cmauch at gmail.com>

package IkiWiki::Plugin::refmeta;

use warnings;
use strict;
use IkiWiki 3.00;

sub import {
	hook(type => "getsetup", id => "refmeta", call => \&getsetup);
	hook(type => "preprocess", id => "refmeta", call => \&preprocess);
	hook(type => "pagetemplate", id => "refmeta", call => \&pagetemplate);
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => 1,
		},
}

my %pagerefmeta;

sub preprocess (@) {
	my %params=@_;

	my $page=$params{page};
	return "" unless $page eq $params{destpage};
	
	if (! defined $params{content}) {
		$pagerefmeta{$page}=undef;
	}
	else {
		my $file = $pagesources{$page};
		my $type = pagetype($file);

		$pagerefmeta{$page}=
			IkiWiki::htmlize($page, $page, $type,
			IkiWiki::linkify($page, $page,
			IkiWiki::preprocess($page, $page, $params{content})));
	}

	return "";
}

my $oldfile;
my $oldcontent;

sub refmeta_content ($) {
	my $page=shift;
	
	return delete $pagerefmeta{$page} if defined $pagerefmeta{$page};

	my $refmeta_page=bestlink($page, "refmeta") || return;
	my $refmeta_file=$pagesources{$refmeta_page} || return;
	my $refmeta_type=pagetype($refmeta_file);
	
	if (defined $refmeta_type) {
		# FIXME: This isn't quite right; it won't take into account
		# adding a new refmeta page. So adding such a page
		# currently requires a wiki rebuild.
		add_depends($page, $refmeta_page);

		my $content;
		if (defined $oldfile && $refmeta_file eq $oldfile) {
			$content=$oldcontent;
		}
		else {
			$content=readfile(srcfile($refmeta_file));
			$oldcontent=$content;
			$oldfile=$refmeta_file;
		}

		return unless length $content;
		return IkiWiki::htmlize($refmeta_page, $page, $refmeta_type,
		       IkiWiki::linkify($refmeta_page, $page,
		       IkiWiki::preprocess($refmeta_page, $page,
		       IkiWiki::filter($refmeta_page, $page, $content))));
	}

}

sub pagetemplate (@) {
	my %params=@_;

	my $template=$params{template};
	if ($params{destpage} eq $params{page} &&
	    $template->query(name => "refmeta")) {
		my $content=refmeta_content($params{destpage});
		if (defined $content && length $content) {
		        $template->param(refmeta => $content);
		}
	}
}

1
