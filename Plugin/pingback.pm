#!/usr/bin/perl
# Sidebar plugin.
# by Tuomo Valkonen <tuomov at iki dot fi>
# Tweaked for pingbacks, no big changes other than name
# Charles Mauch <cmauch at gmail.com>

package IkiWiki::Plugin::pingback;

use warnings;
use strict;
use IkiWiki 3.00;

sub import {
	hook(type => "getsetup", id => "pingback", call => \&getsetup);
	hook(type => "preprocess", id => "pingback", call => \&preprocess);
	hook(type => "pagetemplate", id => "pingback", call => \&pagetemplate);
}

sub getsetup () {
	return
		plugin => {
			safe => 1,
			rebuild => 1,
		},
}

my %pagepingback;

sub preprocess (@) {
	my %params=@_;

	my $page=$params{page};
	return "" unless $page eq $params{destpage};
	
	if (! defined $params{content}) {
		$pagepingback{$page}=undef;
	}
	else {
		my $file = $pagesources{$page};
		my $type = pagetype($file);

		$pagepingback{$page}=
			IkiWiki::htmlize($page, $page, $type,
			IkiWiki::linkify($page, $page,
			IkiWiki::preprocess($page, $page, $params{content})));
	}

	return "";
}

my $oldfile;
my $oldcontent;

sub pingback_content ($) {
	my $page=shift;
	
	return delete $pagepingback{$page} if defined $pagepingback{$page};

	my $pingback_page=bestlink($page, "pingback") || return;
	my $pingback_file=$pagesources{$pingback_page} || return;
	my $pingback_type=pagetype($pingback_file);
	
	if (defined $pingback_type) {
		# FIXME: This isn't quite right; it won't take into account
		# adding a new pingback page. So adding such a page
		# currently requires a wiki rebuild.
		add_depends($page, $pingback_page);

		my $content;
		if (defined $oldfile && $pingback_file eq $oldfile) {
			$content=$oldcontent;
		}
		else {
			$content=readfile(srcfile($pingback_file));
			$oldcontent=$content;
			$oldfile=$pingback_file;
		}

		return unless length $content;
		return IkiWiki::htmlize($pingback_page, $page, $pingback_type,
		       IkiWiki::linkify($pingback_page, $page,
		       IkiWiki::preprocess($pingback_page, $page,
		       IkiWiki::filter($pingback_page, $page, $content))));
	}

}

sub pagetemplate (@) {
	my %params=@_;

	my $template=$params{template};
	if ($params{destpage} eq $params{page} &&
	    $template->query(name => "pingback")) {
		my $content=pingback_content($params{destpage});
		if (defined $content && length $content) {
		        $template->param(pingback => $content);
		}
	}
}

1
