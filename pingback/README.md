## IkiWiki and Pingbacks

Pingbacks are a more modern version of the Trackback system utilized by the
[MovableType](http://www.movabletype.org/) blog system.  The idea was probably
inspired somewhat by the discussion on the meatball wiki regarding
[AutomaticLinkBacks][mbalb] and [BiDirectionalLinks][mbbdl]

### The Pingback Server

The pingback server works by editing the IkiWiki template to point to a
stand-alone version of the pingback [server.pl][2].  Just add something like
the following to your template.

    <TMPL_UNLESS DYNAMIC><link rel="pingback" href="http://point-defiance.rhcloud.com/ping/server.pl"></TMPL_UNLESS>

Any incoming pingback requests are handled by the standalone server, and the
proper xml-rpc responses are generated.  If a pingback is confirmed as valid, a
markdown link is created in a file called `pingback.mdwn` in an IkiWiki
Underlay directory corresponding to the original article.

For example, if your url being pinged looks like `http://foo.com/bar/`, your
original markdown directory tree would either look like `WikiText/bar.mdwn` or
`WikiText/bar/index.mdwn`.  For our purposes, either works.  A pingback underlay
directory exists alongside of your Wiki repository, which is unmaintained by
git.  So a pingback to the above url would be written to
`Underlay/bar/pingback.mdwn`.

By tweaking the IkiWiki sidebar template, (basicly doing a search/replace for
everything in it from sidebar to pingback), we now have a way to automatically
include pingbacks into the wiki text.  You should also ensure any pagespecs in
IkiWiki exclude pingback.mdwn from being included in rss feeds and inclusions
to make everything pretty.

The pingback plugin for ikiwiki can be find in
[pingback.pm][1] and is activated by enabling it in your `ikiwiki.setup` file,
by adding something like the following to your configuration:

    add_plugins:
     - underlay
     - pingback

...

    # pingback plugin
    # show pingback page on all pages?
    global_pingbacks: 0

...

    add_underlays:
    - /home/defiance/Wiki/Pingbacks/

The sourcecode for the modified server can be found in
[server.pl][2].  You *will* need to edit this file's default paths for where it
writes to the underlay directory.  I use openshift to host my wiki, so replace
the openshift environment variables with something that makes sense to you.

### The Pingback Client

This was only slightly modified from it's original sources.  Generally, you'll
create a page on your IkiWiki website with a html form which calls the client.
I use the following markdown:

    This form is used to scan a url (on this site), parse the page for any outgoing
    links and if found, attempt to send a pingback to that blog.  This tool
    provided by <http://software.hixie.ch/utilities/cgi/pingback-proxy/> and tweaked a
    little to prevent linking back to itself.  At some point I'll automate this process
    but for now it works fine.

      <div class="pingbackform">
      <form action="client.pl" method="POST" enctype="multipart/form-data">
       <p>Permalink (on this site!): <input type="text" name="permalink" size="80"></p>
       <p>Manually Enter HTML to Scan instead of link above: <textarea name="content" rows="10" cols="80"></textarea></p>
       <p class="right">(If no content is provided, the given URI will be pinged instead.)</p>
       <p><input type="submit" value="Pingback"></p>
      </form>
      </div>

In the form, you simply paste a url for a page you recently created and
published in IkiWiki.  The script parses the html output, looking for external
links.  It creates an XML-RPC pingback request for every link it finds with a
valid pingback response, and pings the remote site.  Depending on how the
remote site being linked to is configured, it will immediately create a
pingback to the page you created linking to it or sit in a moderation queue and
the author will approve the pingback after reviewing your pages text.

The source for the client can be found in [client.pl][3].  It will probably
require some tweaking on your end to reflect your own URI structure.  The
exceptions should be relatively self-explanatory for anyone with a bit of
regexp and perl knowhow.


[mbbdl]: http://meatballwiki.org/wiki/BidirectionalLink
    "Meatball Wiki: BiDirectional Links"
[mbalb]: http://meatballwiki.org/wiki/AutomaticLinkBack
    "Meatball Wiki: Automatic Linkbacks"

[1]: https://bitbucket.org/PointDefiance/ikiwiki/src/master/Plugin/pingback.pm
    "pingback.pm"
[2]: https://bitbucket.org/PointDefiance/ikiwiki/src/master/pingback/server.pl
    "server.pl"
[3]: https://bitbucket.org/PointDefiance/ikiwiki/src/master/pingback/client.pl
    "client.pl"
